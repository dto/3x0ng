(defpackage #:3x0ng-core
  (:use #:cl #:quadrille)
  (:export *arena-class* arena thing ball wall brick robot barrier bumper goal thief player-1 
	   *use-music* *use-fortresses* *use-bumpers* *difficult* *variation* *arena* *player-1* *player-2* *goal-1* *goal-2* *barrier-1* *barrier-2* *game-length* *game-clock* *score-1* *score-2* *ball* *serve-period-timer* *reset-clock* *walls* *robots* *goals* *barriers* *width* *height* *unit* *3x0ng-copyright-notice* vertex-color drawn-image *player-1-color* *player-2-color* goal-1 goal-2 player-1 player-2
	   either-goal-flashing-p both-joysticks-connected fortresses-p bumpers-p difficult-p configure-game
	   player-1 player-2 reset-game-clock game-on-p game-clock game-clock-string reset-score
	   at-rest-p update-game-clock slow-p ball make-game
	   *draw-box-function* *draw-string-function* *draw-image-function*
	   ))

