;; (in-package :3x0ng)

;; (defgeneric knock-toward-center (thing)
;;   (:generic-function-class inlined-generic-function:inlined-generic-function))

;; (defgeneric restrict-to-buffer (thing)
;;   (:generic-function-class inlined-generic-function:inlined-generic-function))

;; (defgeneric handle-collision (a b)
;;   (:generic-function-class inlined-generic-function:inlined-generic-function))

;; (defgeneric max-speed (thing)
;;   (:generic-function-class inlined-generic-function:inlined-generic-function))

;; (defgeneric max-acceleration (thing)
;;   (:generic-function-class inlined-generic-function:inlined-generic-function))

;; (defgeneric heading-to-center (thing)
;;   (:generic-function-class inlined-generic-function:inlined-generic-function))

;; (defgeneric movement-heading (thing)
;;   (:generic-function-class inlined-generic-function:inlined-generic-function))

;; (defgeneric current-heading (thing)
;;   (:generic-function-class inlined-generic-function:inlined-generic-function))

;; (defgeneric thrust-x (thing)
;;   (:generic-function-class inlined-generic-function:inlined-generic-function))

;; (defgeneric thrust-y (thing)
;;   (:generic-function-class inlined-generic-function:inlined-generic-function))

;; (defgeneric reset-physics (thing)
;;   (:generic-function-class inlined-generic-function:inlined-generic-function))

;; (defgeneric impel (thing &key speed heading)
;;   (:generic-function-class inlined-generic-function:inlined-generic-function))

;; (defgeneric repel (a b &optional speed)
;;   (:generic-function-class inlined-generic-function:inlined-generic-function))

;; (defgeneric update-physics (thing)
;;   (:generic-function-class inlined-generic-function:inlined-generic-function))

;; (defgeneric update (thing)
;;   (:generic-function-class inlined-generic-function:inlined-generic-function))

;; (defgeneric draw (thing)
;;   (:generic-function-class inlined-generic-function:inlined-generic-function))

;; (defgeneric go-to (thing x y)
;;   (:generic-function-class inlined-generic-function:inlined-generic-function))

;; (defgeneric collide (a b)
;;   (:generic-function-class inlined-generic-function:inlined-generic-function))

;; (defgeneric find-goal (thing)
;;   (:generic-function-class inlined-generic-function:inlined-generic-function))

;; (defgeneric carrying-ball-p (thing)
;;   (:generic-function-class inlined-generic-function:inlined-generic-function))

;; (defgeneric humanp (thing)
;;   (:generic-function-class inlined-generic-function:inlined-generic-function))

;; (defgeneric carry-location (thing)
;;   (:generic-function-class inlined-generic-function:inlined-generic-function))

;; (defgeneric lose-ball (thing)
;;   (:generic-function-class inlined-generic-function:inlined-generic-function))

;; (defgeneric grab (thing)
;;   (:generic-function-class inlined-generic-function:inlined-generic-function))

;; (defgeneric find-ball-waypoint (thing)
;;   (:generic-function-class inlined-generic-function:inlined-generic-function))

;; (defgeneric waypoint (thing)
;;   (:generic-function-class inlined-generic-function:inlined-generic-function))

;; (defgeneric heading-to-opponent (thing)
;;   (:generic-function-class inlined-generic-function:inlined-generic-function))

;; (defgeneric heading-to-ball (thing)
;;   (:generic-function-class inlined-generic-function:inlined-generic-function))

;; (defgeneric distance-to-opponent (thing)
;;   (:generic-function-class inlined-generic-function:inlined-generic-function))

;; (defgeneric distance-to-ball (thing)
;;   (:generic-function-class inlined-generic-function:inlined-generic-function))

;; (defgeneric movement-heading (thing)
;;   (:generic-function-class inlined-generic-function:inlined-generic-function))

;; (defgeneric trajectory-heading (thing)
;;   (:generic-function-class inlined-generic-function:inlined-generic-function))

;; (defgeneric fast-p (thing)
;;   (:generic-function-class inlined-generic-function:inlined-generic-function))

;; (defgeneric stick-heading (thing)
;;   (:generic-function-class inlined-generic-function:inlined-generic-function))

;; (defgeneric can-reach-ball (thing)
;;   (:generic-function-class inlined-generic-function:inlined-generic-function))

;; (defgeneric ball-centered-p (thing)
;;   (:generic-function-class inlined-generic-function:inlined-generic-function))

;; (defgeneric kicking-p (thing)
;;   (:generic-function-class inlined-generic-function:inlined-generic-function))

;; (defgeneric ready-to-kick-p (thing)
;;   (:generic-function-class inlined-generic-function:inlined-generic-function))

;; (defgeneric ball-within-range-p (thing)
;;   (:generic-function-class inlined-generic-function:inlined-generic-function))

;; (defgeneric opponent-within-range-p (thing)
;;   (:generic-function-class inlined-generic-function:inlined-generic-function))

;; (defgeneric opponent-carrying-p (thing)
;;   (:generic-function-class inlined-generic-function:inlined-generic-function))

;; (defgeneric kick (thing)
;;   (:generic-function-class inlined-generic-function:inlined-generic-function))

;; (defgeneric zonep (thing)
;;   (:generic-function-class inlined-generic-function:inlined-generic-function))

;; (defgeneric eject (thing)
;;   (:generic-function-class inlined-generic-function:inlined-generic-function))

;; (defgeneric opponent-within-range-p (thing)
;;   (:generic-function-class inlined-generic-function:inlined-generic-function))

