;;; 3x0ng.lisp --- the sequel to 2x0ng

;; Copyright (C) 2014-2016 David O'Toole

;; Author: David O'Toole <dto@xelf.me>
;; Keywords: games

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public License
;; as published by the Free Software Foundation, either version 3 of
;; the License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Lesser General Public License for more details.

;; You should have received a copy of the GNU Lesser General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(in-package :3x0ng)

(defvar *test-quadtree-depth* 4)

(defvar *bricks-p* nil)
(defvar *test-p* nil)

(defvar *draw-box-function* nil)
(defvar *draw-string-function* nil)
(defvar *draw-image-function* nil)

(defmacro %draw-box (x y width height color)
  `(draw-textured-rectangle ,x ,y 0.0 ,width ,height "_box.png" :vertex-color ,color))

(defmacro %draw-string (&rest args) `(funcall *draw-string-function* ,@args))

(defmacro %draw-textured-rectangle (x y width height image color)
  `(draw-textured-rectangle-* ,x ,y 0.0 ,width ,height ,image :vertex-color ,color))

(defmacro percent-of-time (percent &body body)
  `(when (< (random 100.0) ,percent)
     ,@body))

(defun random-choose (set)
  (nth (random (length set)) set))

(defvar *arena-class* nil)

(defun play-sample* (sample) nil)
  ;;(play-sample sample))

(defparameter *color-sounds* '("color1.wav" "color2.wav" "color3.wav"))
(defparameter *whack-sounds* '("whack1.wav" "whack2.wav" "whack3.wav"))
(defparameter *bounce-sounds* '("boop1.wav" "boop2.wav" "boop3.wav"))

(defparameter *doorbell-sounds* '("doorbell1.wav" "doorbell2.wav" "doorbell3.wav"))
(defparameter *slam-sounds* '("slam1.wav" "slam2.wav" "slam3.wav"))

;;; Options, arena, players, and parameters  

(defvar *use-music* nil)
(defvar *use-fortresses* nil)
(defvar *use-bumpers* nil)
(defvar *difficult* nil)

(defun fortresses-p () *use-fortresses*)
(defun bumpers-p () *use-bumpers*)
(defun difficult-p () *difficult*)

(defvar *variation* 2)
(defparameter *variations* 
  '(() 
    (:fortresses) 
    (:bumpers) 
    (:fortresses :bumpers)
    (:difficult)
    (:fortresses :difficult)
    (:bumpers :difficult)
    (:fortresses :bumpers :difficult)))

(defun variation-features (n)
  (let ((index (mod (1- n) (length *variations*))))
    (nth index *variations*)))

(defun configure-game (&optional (variation *variation*))
  (setf *use-fortresses* nil)
  (setf *use-bumpers* nil)
  (setf *difficult* nil)
  (dolist (feature (variation-features variation))
    (case feature
      (:difficult (setf *difficult* t))
      (:bumpers (setf *use-bumpers* t))
      (:fortresses (setf *use-fortresses* t)))))

(defvar *arena* nil)
(defun arena () *arena*)
(defvar *player-1* nil)
(defvar *player-2* nil)
(defvar *goal-1* nil)
(defvar *goal-2* nil)
(defun player-1 () *player-1*)
(defun player-2 () *player-2*)
(defun set-player-1 (x) (setf *player-1* x))
(defun set-player-2 (x) (setf *player-2* x))

(defvar *barrier-1* nil)
(defvar *barrier-2* nil)
(defun barrier-1 () *barrier-1*)
(defun barrier-2 () *barrier-2*)
(defun set-barrier-1 (x) (setf *barrier-1* x))
(defun set-barrier-2 (x) (setf *barrier-2* x))

(defun player-1-p (x) (eq (find-object x) (find-object *player-1*)))
(defun player-2-p (x) (eq (find-object x) (find-object *player-2*)))
(defun goal-1 () *goal-1*)
(defun goal-2 () *goal-2*)
(defun set-goal-1 (x) (setf *goal-1* x))
(defun set-goal-2 (x) (setf *goal-2* x))
(defun goal-1-p (x) (eq (find-object x) (find-object *goal-1*)))
(defun goal-2-p (x) (eq (find-object x) (find-object *goal-2*)))
(defun opponent (x) (cond ((player-1-p x) (player-2)) 
			  ((player-2-p x) (player-1))))

(defun either-goal-flashing-p ()
  (or (slot-value (goal-1) 'timer)
      (slot-value (goal-2) 'timer)))

(defparameter *width* 1280)
(defparameter *height* 720)
(defparameter *unit* 20)
(defun units (n) (* n *unit*))

(defparameter *player-1-color* "hot pink")
(defparameter *player-2-color* "orange")
(defparameter *neutral-color* "white")
(defparameter *arena-color* "yellow green")
(defparameter *wall-color* "gray20")

;;; Finding all objects of a given class in a buffer

(defun find-instances (buffer class-name)
  (let ((objects (objects buffer)))
    (when objects
      (loop for thing being the hash-keys in objects
	 when (typep (find-object thing t) (find-class class-name))
	 collect (find-object thing t)))))

;;; Game clock and scoring

(defparameter *game-length* (minutes 4))

(defvar *game-clock* 0)

(defun reset-game-clock ()
  (setf *game-clock* (if *use-fortresses* (minutes 4) *game-length*)))

(defun update-game-clock ()
  (when (plusp *game-clock*)
    (decf *game-clock*)))

(defun game-on-p () (plusp *game-clock*))

(defun game-clock () *game-clock*)

(defun game-clock-string (&optional (clock *game-clock*))
  (let ((minutes (/ clock (minutes 1)))
	(seconds (/ (rem clock (minutes 1)) 
		    (seconds 1))))
    (format nil "~D~A~2,'0D" (truncate minutes) ":" (truncate seconds))))

(defvar *score-1* 0)
(defvar *score-2* 0)

(defun reset-score () (setf *score-1* 0 *score-2* 0))

;;; Physics 

(defclass thing (quadrille:quadrille)
  ((vertex-color :initform *neutral-color*)
   (drawn-image :initform "_box.png" :accessor drawn-image)
   (heading :initform 0.0)
   ;; thrust magnitudes
   (tx :initform 0.0)
   (ty :initform 0.0)
   ;; physics vars
   (dx :initform 0.0)
   (dy :initform 0.0)
   (ddx :initform 0.0)
   (ddy :initform 0.0)
   ;; physics params
   (max-dx :initform 100)
   (max-dy :initform 100)
   (max-ddx :initform 4)
   (max-ddy :initform 4)))

(defun at-rest-p (thing)
  (with-slots (dx dy) thing
    (and (> *dead-zone* (abs dx))
	 (> *dead-zone* (abs dy)))))

(defun slow-p (thing)
  (with-slots (dx dy) thing
    (and (> 1 (abs dx))
	 (> 1 (abs dy)))))

(defmacro with-center (vars form &body body)
  (let ((object (gensym)))
    `(let ((,object ,form))
       (with-slots (x y width height) ,object
	 (let ((,(first vars)
		(+ x (* 0.5 width)))
	       (,(second vars)
		(+ y (* 0.5 height))))
	   ,@body)))))

(defmethod knock-toward-center ((thing thing))
  (with-center (gx gy) thing
    (with-center (cx cy) (current-buffer)
      (let ((jerk-distance (/ (distance cx cy gx gy) 16)))
	(with-slots (heading) thing
	  (setf heading (find-heading gx gy cx cy))
	  (move thing heading jerk-distance))))))

(defmethod restrict-to-buffer ((thing thing))
  (unless (bounding-box-contains (multiple-value-list (bounding-box (current-buffer)))
				 (multiple-value-list (bounding-box thing)))
    (reset-physics thing)
    (knock-toward-center thing)))

(defmethod handle-collision ((u thing) (v thing))
  (collide u v)
  (collide v u))

(defmethod max-speed ((thing thing)) (slot-value thing 'max-dx))
(defmethod max-acceleration ((thing thing)) (slot-value thing 'max-ddx))

(defparameter *thrust* 0.3)
(defparameter *dead-zone* 0.1)

(defmethod center-of-arena ()
  (values (/ *width* 2) (/ *height* 2)))

(defmethod heading-to-center ((thing thing))
  (with-center (tx ty) thing
    (with-center (cx cy) (current-buffer)
      (find-heading tx ty cx cy))))
  
(defun clamp (x bound)
  (max (- bound)
       (min x bound)))

(defun clamp0 (x bound)
  (let ((value (clamp x bound)))
    (if (< (abs value) *dead-zone*)
	0
	value)))

(defun decay (x)
  (let ((z (* 0.94 x)))
    z))

(defmethod movement-heading ((self thing)) nil)

(defmethod current-heading ((self thing)) 
  (slot-value self 'heading))

(defmethod thrust-x ((self thing)) 
  (when (movement-heading self) *thrust*))

(defmethod thrust-y ((self thing)) 
  (when (movement-heading self) *thrust*))
      
(defmethod reset-physics ((self thing))
  (with-slots (dx dy ddx ddy) self
    (setf dx 0 dy 0 ddx 0 ddy 0)))

(defmethod impel ((self thing) &key speed heading)
  (play-sample* (random-choose *whack-sounds*))
  (with-slots (tx ty dx dy ddx ddy) self
    (setf (slot-value self 'heading) heading)
    (setf ddx 0 ddy 0)
    (setf dx (* speed (cos heading)))
    (setf dy (* speed (sin heading)))))
          
(defmethod repel ((this thing) (that thing) &optional (speed 30))
  (impel that :speed speed :heading (heading-between this that)))

(defmethod update-physics ((self thing))
  (unless (eq :passive (slot-value self 'collision-type))
    (with-slots (x y dx dy ddx ddy tx ty heading
		   max-dx max-ddx max-dy max-ddy) self
      (let ((thrust-x (thrust-x self))
	    (thrust-y (thrust-y self)))
	(setf tx (if thrust-x (* thrust-x (cos heading)) nil))
	(setf ty (if thrust-y (* thrust-y (sin heading)) nil))
	(setf ddx (clamp (or tx (decay ddx))
			 (max-acceleration self)))
	(setf dx (clamp (if tx (+ dx ddx) (decay dx))
			(max-speed self)))
	(setf ddy (clamp (or ty (decay ddy))
			 (max-acceleration self)))
	(setf dy (clamp (if ty (+ dy ddy) (decay dy))
			(max-speed self)))
	(move-to self 
		 (+ x dx)
		 (+ y dy))
	(setf heading (or (movement-heading self) heading))))))

(defmethod update :before ((thing thing))
  (unless (eq :passive (collision-type thing))
    (update-physics thing)))
	   	   
;;; The bouncing Squareball

(defparameter *ball-size* (units 0.5))
(defparameter *kick-disabled-time* 40)

(defvar *ball* nil)
(defun ball () *ball*)

(defun random-serve-heading ()
  (direction-heading (random-choose '(:up :down))))

(defun image-set (name count &optional (start 1))
  (loop for n from start to count
	collect (format nil "~A-~S.png" name n)))

(defparameter *ball-images* (image-set "ball" 6))

(defun random-ball-image () "ball-5.png")
  ;; (random-choose *ball-images*))

(defclass ball (thing)
  ((max-dx :initform 100)
   (max-dy :initform 100)
   (max-ddx :initform 0.01)
   (max-ddy :initform 0.01)
   (drawn-image :initform (random-ball-image))
   (kick-clock :initform 0)
   (vertex-color :initform *neutral-color*)
   (count :initform 0) 
   (heading :initform (random-serve-heading))))

(defmethod go-to ((ball ball) x y)
  (move-to ball (- x (/ *ball-size* 2)) (- y (/ *ball-size* 2))))

(defmethod initialize-instance :after ((ball ball) &key)
  (setf *ball* ball)
  (resize ball *ball-size* *ball-size*))

(defmethod paint ((ball ball) vertex-color)
  (setf (slot-value ball 'vertex-color) vertex-color))

(defmethod disable-kicking ((ball ball))
  (setf (slot-value ball 'kick-clock) *kick-disabled-time*))

(defmethod recently-kicked-p ((ball ball))
  (plusp (slot-value ball 'kick-clock)))

(defmethod bounce ((ball ball) &optional (speed 20))
  (with-slots (heading) ball
    (free-ball)
    (reset-physics ball)
    (setf heading (opposite-heading heading))
    (move ball heading 10)
    (impel ball :speed speed :heading heading)))

(defmethod update ((ball ball))
  (when (not (zonep ball))
    (setf (slot-value ball 'count) 0))
  (with-slots (kick-clock) ball
    (when (plusp kick-clock)
      (decf kick-clock))
    (restrict-to-buffer ball))
  (let ((carrier (ball-carrier)))
    (when carrier
      (multiple-value-bind (x y) 
	  (carry-location carrier)
	(go-to ball x y))
      ;; don't allow camping in own end zone
      (when (zonep carrier)
	(free-ball)
	(eject ball)))))

;;; Walls and bricks

(defclass wall (thing)
  ((vertex-color :initform *wall-color*)))

(defmethod collide ((ball ball) (wall wall))
  (unless (ball-carrier) 
    (bounce ball)))

(defparameter *brick-width* (units 1.8))
(defparameter *brick-height* (units 1.2))

(defclass brick (thing)
  ((collision-type :initform :passive)
   (vertex-color :initform "white")
   (height :initform *brick-height*)
   (width :initform *brick-width*)))

(defmethod bounding-box ((brick brick))
  (with-slots (x y width height) brick
    (values 
     (cfloat (+ y 2))
     (cfloat (+ x 2))
     (cfloat (+ x width -2))
     (cfloat (+ y height -2)))))

(defmethod handle-collision ((this brick) (that brick)) nil)

(defmethod collide ((ball ball) (brick brick))
  (with-slots (count) ball
    (if (< count 5)
	(progn (destroy brick)
	       (play-sample* (random-choose *color-sounds*))
	       (bounce ball 10)
	       (incf count))
	(eject ball))))

(defun make-brick (x y &optional (vertex-color "cyan"))
  (let ((brick (make-instance 'brick)))
    (assert (slot-boundp brick 'uuid))
    (resize brick *brick-width* *brick-height*)
    (move-to brick x y)
    (setf (slot-value brick 'vertex-color) vertex-color)
    brick))

(defun make-column (x y count &optional (vertex-color "cyan"))
  (dotimes (n count)
    (add-node (current-buffer) (make-brick x y vertex-color) x y)
    (incf y *brick-height*)))

(defparameter *fortress-height* 28)

(defun make-fortress (x y colors)
  (dolist (color colors)
    (make-column x y *fortress-height* color)
    (incf x *brick-width*)))

(defparameter *player-1-fortress-colors* '("dark orchid" "medium orchid" "orchid"))

(defparameter *player-2-fortress-colors* '("dark orange" "orange" "gold"))

;;; A player bot, either human or AI controlled

(defparameter *max-speed* 3.9)
(defparameter *max-carry-speed* 3.5)

(defvar *serve-period-timer* 0)
(defparameter *serve-period* 55)

(defun update-serve-period-timer ()
  (when (plusp *serve-period-timer*)
    (decf *serve-period-timer*)))

(defun serve-period-p ()
  (plusp *serve-period-timer*))

(defun begin-serve-period ()
  (setf *serve-period-timer* *serve-period*))

(defclass robot (thing)
  ((max-dx :initform *max-speed*)
   (max-dy :initform *max-speed*)
   (max-ddx :initform 1.5)
   (max-ddy :initform 1.5)
   (input-heading :initform nil :accessor input-heading)
   (input-kicking-p :initform nil :accessor input-kicking-p)
   (drawn-image :initform "robot.png")
   ;; (vertex-color :initform *player-1-color*)
   (carrying :initform nil)
   (kick-clock :initform 0)))

(defmethod find-score ((robot robot)) (slot-value robot 'score))

(defmethod score-point (player)
  (ecase player
    (1 (incf *score-1*) (play-fanfare-1))
    (2 (incf *score-2*) (play-fanfare-2))))

(defmethod find-goal ((robot robot)) (goal-2))

(defmethod carrying-ball-p ((robot robot))
  (slot-value robot 'carrying))

(defmethod max-speed ((robot robot))
  (if (carrying-ball-p robot) 
      *max-carry-speed* 
      (slot-value robot 'max-dx)))

(defun find-robots ()
  (ensure-collision-lists)
  *robots*)

(defun ball-carrier ()
  (if (carrying-ball-p (player-1))
      (player-1)
      (if (carrying-ball-p (player-2))
	  (player-2))))

(defparameter *traditional-robot-colors* '("gold" "olive drab" "RoyalBlue3" "dark orchid"))

(defparameter *robot-size* 20)

(defmethod initialize-instance :after ((robot robot) &key)
  (resize robot *robot-size* *robot-size*))

(defmethod humanp ((robot robot)) nil)

(defparameter *robot-reload-frames* 30)

;;; Can't pass through walls

(defmethod collide ((wall wall) (robot robot))
  (impel robot :speed 10 :heading (heading-to-center robot)))

(defmethod collide ((brick brick) (robot robot))
  (impel robot :speed 10 :heading (heading-to-center robot)))

;;; Grab ball when you collide with it

(defmethod lose-ball ((robot robot))
  (with-slots (carrying) robot
    (setf carrying nil)))

(defun free-ball ()
  (when (game-on-p) (play-rhythm))
  (lose-ball (player-1))
  (lose-ball (player-2)))

(defmethod grab ((robot robot))
  (when (and (not (recently-kicked-p (ball)))
	     (ready-to-kick-p robot))
    (free-ball) 
    (play-sample* (random-choose *doorbell-sounds*))
    (with-slots (carrying) robot
      (setf carrying t))))

(defmethod collide ((robot robot) (ball ball))
  (unless (or (ball-carrier) (recently-kicked-p ball))
    (grab robot)))

;;; Glue the ball to the ball carrier, with some wobble

(defun wobble () (sin (/ *updates* 10)))

(defmethod carry-location ((robot robot))
  (with-slots (heading) robot
    (with-center (cx cy) robot
      (multiple-value-bind (tx ty) 
	  (step-coordinates cx cy heading (units 2))
	(multiple-value-bind (wx wy)
	    (step-coordinates tx ty (- heading (/ pi 2)) (* 30 (wobble)))
	  (values (- wx (* *ball-size* 0.12))
		  (- wy (* *ball-size* 0.12))))))))

;;; Default AI methods and tools

(defparameter *waypoint-margin* (units 4))

(defmethod find-ball-waypoint ((robot robot))
  (with-center (cx cy) (arena)
    (let ((rx (center-point robot))
	  (bx (center-point (ball)))
	  (left (- cx *waypoint-margin* (units 2)))
	  (right (+ cx *waypoint-margin* (units 2)))
	  (b1x (center-point (barrier-1)))
	  (b2x (center-point (barrier-2))))
      (if (and (zonep (ball))
	       (slow-p (ball)))
	  (cond ((< (abs (- rx b1x))
		    (abs (- rx bx)))
		 (values (- cx *waypoint-margin*) cy))		       
		((< (abs (- rx b2x))
		    (abs (- rx bx)))
		 (values (+ cx *waypoint-margin*) cy))
		(t (center-point (ball))))
	  (if (bumpers-p)
	      (cond ((and (< left rx right) (< (- cy *waypoint-margin*)
					       rx
					       (+ cy *waypoint-margin*)))
		     (center-point (find-goal robot)))
		    ;;
		    ((< bx cx right rx)
		     (values (+ cx *waypoint-margin*) cy))
		    ((< left bx cx rx)
		     (values (- cx *waypoint-margin*) cy))
		    ;;
		    ((< rx left cx bx)
		     (values (- cx *waypoint-margin*) cy))
		    ((< rx cx bx right)
		     (values (+ cx *waypoint-margin*) cy))
		    (t (center-point (ball))))
	      (center-point (ball)))))))

(defmethod waypoint ((robot robot))
  (with-center (ax ay) (arena)
    (let ((cx (center-point robot)))
      (if (not (carrying-ball-p robot))
	  (find-ball-waypoint robot)
	  (if (bumpers-p)
	      ;; navigate around bumper
	      (cond ((> cx (+ ax *waypoint-margin*))
		     (values (+ ax *waypoint-margin*) ay))
		    ((> cx ax)
		     (values ax ay))
		    (t (center-point (find-goal robot))))
	      (center-point (find-goal robot)))))))

(defmethod heading-to-opponent ((robot robot))
  (heading-between robot (opponent robot)))

(defmethod heading-to-ball ((robot robot))
  (if (ball) (heading-between robot (ball)) 0))

(defmethod distance-to-opponent ((robot robot))
  (distance-between robot (opponent robot)))

(defmethod distance-to-ball ((robot robot))
  (if (ball) (distance-between robot (ball)) 10000))

(defun jitter (heading)
  (+ heading (* (if (difficult-p) 0.15 0.3) (sin (/ *updates* 24)))))

(defun course-correction ()
  (if (difficult-p) 
      (if (serve-period-p) 0.3 0.2)
      (if (serve-period-p) 0.33 0.22)))

(defmethod movement-heading ((robot robot))
  ;; (if (or *netplay* (both-joysticks-connected))
  ;;     (stick-heading robot)
      (when (and (game-on-p)
		 (not (either-goal-flashing-p)))
	(percent-of-time
	    (if (serve-period-p)
		(if (not (difficult-p)) 55 67)
		(if (and (zonep (ball))
			 (slow-p (ball)))
		    ;; anticipate eject but don't superspeed
		    (if (not (difficult-p)) 23 34)
		    (if (and 
			 (not (carrying-ball-p robot))
			 (at-rest-p (ball))
			 (ball-within-range-p robot))
			;; slow down to catch ball
			(if (slow-p robot) 50 45)
			;; default 
			(if (not (difficult-p)) 65 72))))
		(when (and (not (^colliding-with-p (ball) (goal-1))) 
			   (not (^colliding-with-p (ball) (goal-2))))
		  (with-center (cx cy) robot
		    (multiple-value-bind (wx wy) (waypoint robot)
		      (if (carrying-ball-p robot)
			  (jitter (find-heading cx cy wx wy))
			  (if (fast-p robot)
			      ;; correct path to not overshoot ball
			      (let ((delta (- (find-heading cx cy wx wy)
					      (trajectory-heading robot))))
				(if (plusp delta)
				    (jitter (+ (find-heading cx cy wx wy) (course-correction)))
				    (jitter (- (find-heading cx cy wx wy) (course-correction)))))
			      (jitter (find-heading cx cy wx wy))))))))))
  
(defmethod trajectory-heading ((thing thing))
  (with-slots (x y last-x last-y) thing
    (find-heading last-x last-y x y)))

(defmethod fast-p ((thing thing))
  (with-slots (x y last-x last-y) thing
    (when (and last-x last-y)
      (> (distance last-x last-y x y)
	 3.4))))

(defparameter *player-1-joystick* 0)
(defparameter *player-2-joystick* nil)

(defun both-joysticks-connected ()
  (numberp *player-2-joystick*))

(defmethod stick-heading ((self robot)) nil)

(defmethod can-reach-ball ((self robot))
  (and (ball) (^colliding-with-p self (ball))))

(defmethod ball-centered-p ((robot robot))
  (> 0.4 (abs (wobble))))

(defparameter *robot-shoot-distance* 320)

(defmethod kicking-p ((robot robot))
  (cond ;; ((and (both-joysticks-connected)
	;;       (not *netplay*))
	;;  (holding-button-p *player-2-joystick*))
	((not (game-on-p)) nil)
	((^colliding-with-p (ball) (goal-1)) nil)
	((carrying-ball-p robot)
	 (percent-of-time 
	     (if (< (distance-between robot (find-goal robot)) 240)
		 (if (not (difficult-p)) 25 34)
		 (if (not (difficult-p)) 20 25))
	   (and (ball-centered-p robot)
		(< (distance-between robot (find-goal robot)) 
		   (if (not (difficult-p)) 
		       (+ *robot-shoot-distance* 30)
		       (+ *robot-shoot-distance* 70))))))
	((carrying-ball-p (opponent robot))
	 (percent-of-time (if (not (difficult-p)) 1.2 2.2)
			  (and 
			   (opponent-within-range-p robot)
			   (ball-within-range-p robot))))))
	
(defmethod ready-to-kick-p ((robot robot)) 
  (zerop (slot-value robot 'kick-clock)))

(defparameter *kick-speed* 28)
(defparameter *steal-speed* 32)
(defparameter *kick-range* (units 2.8))

(defmethod ball-within-range-p ((robot robot))
  (< (distance-between robot (ball))
     *kick-range*))

(defparameter *repel-range* (units 4))

(defmethod opponent-within-range-p ((robot robot))
  (< (distance-between robot (opponent robot))
     *repel-range*))

(defmethod opponent-carrying-p ((robot robot))
  (with-slots (carrying) robot
    (and (not carrying)
	 (ball-carrier))))

(defmethod kick ((self robot))
  (with-slots (carrying kick-clock) self
    (play-sample* (random-choose *bounce-sounds*))
    (setf kick-clock *robot-reload-frames*)
    (when (or carrying (ball-within-range-p self))
      (let ((speed (if (opponent-carrying-p self)
		       *steal-speed*
		       *kick-speed*)))
	(when (opponent-carrying-p self)
	  (play-sample* "grab.wav"))
	(free-ball)
	(disable-kicking (ball))
	(impel (ball) 
	       :heading 
	       (if (opponent-carrying-p self)
		   (* 0.5 (+ (heading-to-ball self) 
			     (heading-to-opponent self)))
		   (heading-between self (ball)))
	       :speed speed)
	(play-sample* "serve.wav")
	(when (opponent-within-range-p self)
	  (repel self (opponent self) 12))))))

;; (defmethod kick :after ((self robot))
;;   (when (humanp self)
;;     (hide-terminal)))

(defmethod collide ((this robot) (that robot))
  (repel this that))

;;; Barriers to exclude opponent

(defparameter *barrier-width* (units 0.2))
(defparameter *barrier-height* (- *height* (units 2.2)))

(defclass barrier (thing)
  ((quadrille:collision-type :initform :passive)
   (vertex-color :initform "white")
   (excluded-player :initform nil)))

(defun make-barrier (x y excluded-player)
  (let ((barrier (make-instance 'barrier)))
    (resize barrier *barrier-width* *barrier-height*)
    (move-to barrier x y)
    (setf (slot-value barrier 'excluded-player) excluded-player)
    barrier))

(defmethod collide ((barrier barrier) (robot robot))
  (with-slots (excluded-player) barrier
    (when (eq robot excluded-player)
      (repel barrier excluded-player 20))))

(defmethod zonep ((thing thing))
  (with-center (x y) thing
    (or (< x (slot-value (barrier-1) 'x))
	(> x (slot-value (barrier-2) 'x)))))

(defmethod eject ((thing thing))
  (with-center (gx gy) thing
    (with-center (cx cy) (arena)
      (let ((jerk-distance (/ (distance cx cy gx gy) 20)))
	(with-slots (heading) thing
	  (setf heading (find-heading gx gy cx cy))
	  (impel thing :heading heading :speed jerk-distance))))))

(defmethod eject :after ((ball ball))
  (play-sample* "return.wav"))

(defmethod update :around ((ball ball))
  (unless (either-goal-flashing-p)
    (call-next-method)
    (when (and (at-rest-p ball)
	       (not (ball-carrier))
	       (zonep ball))
      (free-ball)
      (eject ball))))

;;; Bumpers

(defparameter *bumper-width* (units 0.3))
(defparameter *bumper-height* (- (/ *height* 2) (units 5)))

(defclass bumper (thing) 
  ((vertex-color :initform "white")))

(defun make-bumper (x y)
  (let ((bumper (make-instance 'bumper)))
    (resize bumper *bumper-width* *bumper-height*)
    (move-to bumper x y)
    bumper))

(defmethod collide ((bumper bumper) (robot robot))
  (repel bumper robot))

(defmethod collide ((ball ball) (bumper bumper))
  (bounce ball))

;;; Goals

(defclass goal (thing)
  ((colors :initform (list "hot pink" "cyan"))
   (player :initform  nil)
   (timer :initform nil)
   (direction :initform :up)))

(defmethod set-scoring-player ((goal goal) p)
  (with-slots (player colors) goal
    (setf player p)
    (if (= 1 p) 
	(setf colors (list *player-2-color* "yellow"))
	(setf colors (list *player-1-color* "purple")))))

(defmethod collide ((goal goal) (robot robot))
  (impel robot :speed 10 :heading (heading-to-center robot)))

(defmethod collide ((goal goal) (ball ball))
  (when (and (game-on-p)
	     (not (ball-carrier)))
    (reset-physics ball)
    (with-slots (timer player) goal
      (when (null timer)
	(score-point player)
	(play-sample* "newball.wav")
	(setf timer (seconds 3))))))

(defparameter *goal-speed* 1)

(defmethod update ((goal goal))
  (when (game-on-p)
    (with-slots (timer direction) goal
      (when timer
	(when (plusp timer)
	  (decf timer))
	(when (zerop timer)
	  (add-hook '*next-update-hook* #'(lambda () (proceed (arena))))
	  (decf timer)))
      (move goal (direction-heading direction) *goal-speed*))))

(defmethod collide ((goal goal) (wall wall))
  (with-slots (direction) goal
    (setf direction (opposite-direction direction))
    (move-toward goal direction (+ *goal-speed* 5))))

(defun make-goal ()
  (let ((goal (make-instance 'goal)))
    (resize goal (units 1) (units 4))
    goal))

(defmethod clear ((goal goal))
  (setf (slot-value goal 'timer) nil))

(defun clear-goals ()
  (clear (goal-1))
  (clear (goal-2)))

;;; Control logic 

(defmethod update ((self robot))
  (with-slots (kick-clock) self
    (when (plusp kick-clock)
      (decf kick-clock))
    (when (and (ready-to-kick-p self)
	       ;; whether to allow spamming the ball when you don't have it
	       ;; (ball-carrier)
	       (kicking-p self))
      (kick self))))

(defclass thief (robot) 
  ((vertex-color :initform *player-2-color*)
   (player-id :initform 2)))

(defmethod find-goal ((thief thief)) (goal-1))

(defmethod humanp ((self thief)) nil)

;; Player 1 drives the logic with the arrows/numpad and spacebar

(defclass player-1 (robot) 
  ((vertex-color :initform *player-1-color*)
   (player-id :initform 1)))

(defmethod humanp ((self player-1)) t)

(defmethod find-goal ((player-1 player-1)) (goal-2))

;; (defmethod thrust-x ((self player-1)) 
;;   (destructuring-bind (horizontal vertical) (joystick-left-analog-stick)
;;     (when 
;; 	(or (when (not (clientp (arena)))
;; 	      (and (holding-left-arrow-p) 
;; 		   (holding-right-arrow-p)))
;; 	    (joystick-axis-pressed-p horizontal *player-1-joystick*))
;;       *thrust*)))

;; (defmethod thrust-y ((self player-1)) 
;;   (destructuring-bind (horizontal vertical) (joystick-left-analog-stick)
;;     (when 
;; 	(or (when (not (clientp (arena)))
;; 	      (and (holding-up-arrow-p) 
;; 		   (holding-down-arrow-p)))
;; 	    (joystick-axis-pressed-p vertical *player-1-joystick*))
;;       *thrust*)))

;;; The arena and game setup

(defun make-wall (x y width height)
  (let ((wall (make-instance 'wall)))
    (assert (slot-boundp wall 'uuid))
    (resize wall width height)
    (move-to wall x y)
    wall))

(defun make-border (x y width height)
  (let ((left x)
	(top y)
	(right (+ x width))
	(bottom (+ y height)))
    ;; top wall
    (add-node (current-buffer) (make-wall left top (- right left) (units 1)))
    ;; bottom wall
    (add-node (current-buffer) (make-wall left bottom (- right left (units -1)) (units 1)))
    ;; left wall
    (add-node (current-buffer) (make-wall left top (units 1) (- bottom top)))
    ;; right wall
    (add-node (current-buffer) (make-wall right top (units 1) (- bottom top (units -1))))))

(defclass arena (quadrille:qbuffer)
  ((resetting :initform nil)
   (ended :initform nil)
   (quadtree-depth :initform 6)))

(defvar *reset-clock* nil)

(defmethod quit-game ((arena arena)) nil)
  ;;(shut-down))

(defvar *walls* nil)
(defvar *robots* nil)
(defvar *goals* nil)
(defvar *barriers* nil)

(defun ensure-collision-lists ()
  (when (null *walls*)
    (setf *walls* (find-instances (arena) 'wall))
    (setf *robots* (list (player-1) (player-2)))
    (setf *goals* (list (goal-1) (goal-2)))
    (setf *barriers* (list (barrier-1) (barrier-2)))))

  ;; (when (keyboard-down-p :pagedown)
  ;;   (when (null *reset-clock*)
  ;;     (reset-score)
  ;;     (reset-game-clock)
  ;;     (setf *reset-clock* (seconds 1))
  ;;     (at-next-update (reset-game arena)))))

(defun drop-player-1 ()
  (add-node (current-buffer) (player-1) (units 11) (units 3))
  (reset-physics (player-1)))

(defun drop-player-2 ()
  (add-node (current-buffer) (player-2) (- *width* (units 12)) (- *height* (units 4)))
  (reset-physics (player-2)))

(defun drop-ball ()
  (free-ball)
  (reset-physics (ball))
  (with-center (x y) (arena)
    (add-node (arena) (ball) (- x (/ *ball-size* 2)) (- y (/ *ball-size* 2)))))

(defmethod populate ((arena arena))
  (let ((*buffer* arena))
    (with-center (x y) arena
      (add-node (current-buffer) (make-instance 'ball) x y)
      (set-player-1 (make-instance 'player-1))
      (set-player-2 (make-instance 'thief))
      (set-goal-1 (make-goal))
      (set-goal-2 (make-goal))
      (set-scoring-player (goal-1) 2)
      (set-scoring-player (goal-2) 1)
      (add-node (current-buffer) (goal-1) (units 1.1) (units 4))
      (add-node (current-buffer) (goal-2) (- *width* (units 2.1)) (- *height* (units 8)))

      (when (fortresses-p)
      	(make-fortress (units 3) (units 1.2) *player-1-fortress-colors*)
      	(make-fortress (- *width* (units 8.5)) (units 1.2) *player-2-fortress-colors*))

      (when (bumpers-p)
	(add-node (current-buffer) (make-bumper (/ *width* 2) (units 1.2)))
	(add-node (current-buffer) (make-bumper (/ *width* 2) (- *height* *bumper-height* (units 1.1)))))

      (set-barrier-1 (make-barrier 0 0 (player-2)))
      (set-barrier-2 (make-barrier 0 0 (player-1)))
      (add-node (current-buffer) (barrier-1) (units 10) (units 1.1)) 
      (add-node (current-buffer) (barrier-2) (- *width* (units 10.2)) (units 1.1))
      
      (drop-player-1)
      (drop-player-2)
      ;;(setf (slot-value (current-buffer) 'background-color) *arena-color*)
      (begin-serve-period))))

(defun make-game (&optional (variation 2))
  (let ((buffer (make-instance *arena-class*)))
    (switch-to-buffer buffer)
    (configure-game variation)
    (play-sample* "go.wav")
    (play-rhythm)
    (make-border 0 0 (- *width* (units 1)) (- *height* (units 1)))
    ;; (trim (current-buffer))
    (populate buffer)
    (current-buffer)))
   ;; (trim (current-buffer))))

(defun do-reset ()
  (hide-terminal)
  (reset-score)
  (dotimes (n 100)
    (halt-sample n))
  (reset-game-clock)
  (start (make-game *variation*)))

(defmethod reset-game ((self arena))
  (hide-terminal)
  (stop self)
  (do-reset)
  (add-hook '*next-update-hook* #'(lambda () (destroy self))))
  
(defmethod select-variation ((arena arena))
  (hide-terminal)
  (let ((v *variation*))
    (incf v)
    (setf v (mod v (length *variations*)))
    (setf *variation* v)
    (reset-game arena)))

(defmethod toggle-music ((self arena))
  (setf *use-music* (if *use-music* nil t))
  (if *use-music*
      (play-rhythm)
      (halt-music)))

(defparameter *debug* nil)

(defmethod toggle-debug ((self arena))
  (setf *debug* (if *debug* nil t)))

(defmethod proceed ((arena arena))
  (play-sample* "go.wav")
  (drop-ball)
  (drop-player-1)
  (drop-player-2)
  (clear-goals))

(defparameter *3x0ng-copyright-notice*
"
 #####           ###                 
#     # #    #  #   #  #    #  ####  
      #  #  #  #   # # ##   # #    # 
 #####    ##   #  #  # # #  # #      
      #   ##   # #   # #  # # #  ### 
#     #  #  #   #   #  #   ## #    # 
 #####  #    #   ###   #    #  ####  

-----------------------------------------------------------------
Welcome to 3x0ng. 
3x0ng and Xelf are Copyright (C) 2006-2016 by David T. O'Toole 
email: <dto@xelf.me>   website: http://xelf.me/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 

Full license text of the GNU Lesser General Public License is in the
enclosed file named 'COPYING'. Full license texts for compilers,
assets, libraries, and other items are contained in the LICENSES
directory included with this application.
-----------------------------------------------------------------
")

(defun ^colliding-with-p (a b)
  (declare (optimize (speed 3) (safety 1)))
  (with-slots (x y width height) a
    (quadrille::point-in-rectangle-p
     (cfloat x) (cfloat y) (cfloat width) (cfloat height)
     (cfloat (slot-value b 'y))
     (cfloat (slot-value b 'x))
     (cfloat (slot-value b 'width))
     (cfloat (slot-value b 'height)))))

(defun play-rhythm ()
  (when *use-music* (play-music "rhythm.ogg" :loop t)))

(defun play-fanfare-1 () 
  (when *use-music* (play-music "fanfare-1.ogg" :loop nil)))

(defun play-fanfare-2 ()
  (when *use-music* (play-music "fanfare-2.ogg" :loop nil)))

(defun play-end-music ()
  (when *use-music* (play-music "end.ogg" :loop nil)))

(defparameter *score-font* "sans-mono-bold-12")

(defparameter *big-font* "sans-mono-bold-16")

(defmethod %draw ((self thing))
  (with-slots (vertex-color x y width height heading drawn-image) self
    (draw-textured-rectangle x y 0 width height drawn-image :vertex-color vertex-color)))
    ;; (draw-textured-rectangle-* x y 0
    ;; 			       width height
    ;; 			       drawn-image
    ;; 			       :vertex-color vertex-color
    ;; 			       :blend :alpha
    ;; 			       :angle (+ 90 (heading-degrees heading)))))

;; (defmethod %draw :around ((ball ball))
;;   (unless (either-goal-flashing-p)
;;     (call-next-method)))

;; (defmethod %draw ((wall wall))
;;   (with-slots (x y width height vertex-color) wall
;;     (%draw-box x y width height vertex-color)))

;; (defmethod %draw ((brick brick))
;;   (with-slots (x y width height vertex-color) brick
;;     (%draw-box x y width height vertex-color)))

;; (defmethod %draw ((barrier barrier))
;;   (with-slots (x y width height vertex-color) barrier
;;     (%draw-box x y width height vertex-color)))

;; (defmethod %draw ((bumper bumper))
;;   (with-slots (x y width height vertex-color) bumper
;;     (%draw-box x y width height vertex-color)))

;; (defmethod %draw ((goal goal))
;;   (with-slots (x y width height colors timer) goal
;;     (let ((color2 (if timer (random-choose '("hot pink" "magenta" "yellow")) 
;; 		      (first colors))))
;;       (%draw-box x y width height color2))))

(defun do-draw ()
  (%draw-box 0 0 1280 720 "black")
  (if *test-p*
      (time
       (loop for v being the hash-values of (slot-value *buffer* 'objects)
	  do (%draw v)))
      (loop for v being the hash-values of (slot-value *buffer* 'objects)
	 do (%draw v))))

(defmethod quadrille:find-identifier ((thing thing))
  thing)

;; (defmethod %draw :after ((arena arena))
;;   ;; (xelf:draw-terminal)
;;   (%draw-string (format nil "~S" *score-1*)
;; 	       (units 2) 3
;; 	       :color *player-1-color* 
;; 	       :font *score-font*)
;;   (%draw-string (format nil "~S" *score-2*)
;; 	       (- *width* (units 5)) 3
;; 	       :color *player-2-color* 
;; 	       :font *score-font*)
;;   (%draw-string (game-clock-string) 
;; 	       (units 31.6) 3
;; 	       :color "white"
;; 	       :font *score-font*)
;;   (%draw-string "[Arrows/NumPad] move     [Shift] kick      [Escape] game setup     [PageDown] reset game     [PageUp] select variation     [Control-Q] quit      [Control-M] music on/off"
;; 	       (units 2.6) (- *height* 17)
;; 	       :color "white"
;; 	       :font *score-font*)
;;   (unless (game-on-p)
;;     (%draw-string "END OF REGULATION"
;; 		 (units 36) (units 2)
;; 		 :color "white"
;; 		 :font *big-font*))
;;   ;; (when (and (not (both-joysticks-connected))
;;   ;; 	     (not *netplay*))
;;     (%draw-string (if (not (difficult-p)) "NORMAL AI" "ADVANCED AI")
;; 		 (units 48) 3
;; 		 :color *player-2-color*
;; 		 :font *score-font*)
;;   ;; (when *netplay*
;;   ;;   (%draw-string (ecase *netplay*
;;   ;; 		   (:client "CLIENT: PLAYER 2")
;;   ;; 		   (:server "SERVER: PLAYER 1"))
;;   ;; 		 (units 48) 3
;;   ;; 		 :color (ecase *netplay*
;;   ;; 			  (:client *player-2-color*)
;;   ;; 			  (:server *player-1-color*))
;;   ;; 		 :font *score-font*))
;;   ;; draw gray bars under goal slot to prevent color problems
;;   (let ((x1 (slot-value (goal-1) 'x))
;; 	(x2 (slot-value (goal-2) 'x))
;; 	(y (units 1))
;; 	(width (units 1.3))
;; 	(height (units 34)))
;;     (%draw-box (- x1 3) y width height :color "gray30")
;;     (draw (goal-1))
;;     (%draw-box (- x2 3) y width height :color "gray30")
;;     (draw (goal-2))
;;     (draw (ball))
;;     (draw (player-1))
;;     (draw (player-2))))

(defvar *box* nil)

(defclass arena* (xelf:buffer arena)
  ((quadtree-depth :initform *test-quadtree-depth*)))

(setf *arena-class* 'arena*)

(defmethod update :after ((arena arena*))
  ;; (ext:gc nil)
  ;; (when (and *test-p* (= *updates* 50))
  ;;   (incf *updates*)
  ;;   (time (dotimes (n 300)
  ;; 	    (update arena))))
  (ensure-collision-lists)
  (update *ball*)
  (mapc #'update *robots*)
  (mapc #'update *goals*)
  (update-game-clock)
  (update-serve-period-timer)
  (when (zerop *game-clock*)
    (when (null (slot-value arena 'ended))
      (setf (slot-value arena 'ended) t)
      (play-sample* "error.wav")
      (play-end-music)))
  (when *reset-clock*
    (decf *reset-clock*)
    (unless (plusp *reset-clock*)
      (setf *reset-clock* nil))))
  ;; (with-slots (objects) arena
  ;;   (loop for uuid being the hash-keys in objects do
  ;; 	 (let ((object? (search-identifier (gethash uuid objects))))
  ;; 	   (when object?
  ;; 	     (unless (eq :passive (slot-value object? 'collision-type))
  ;; 	       (quadtree-collide object? quadtree)))))))

(defmethod xelf:draw ((thing thing))
  (%draw thing))

(setf xelf::*draw-function*
      #'(lambda ()
	  (setf *box* "_box.png")
	  (do-draw)))

;; (defmethod xelf:draw :around ((arena arena*))
;;   (%draw arena))
	   
;;; Main program

(defparameter *title-string* "3x0ng 1.94")

(defmethod initialize-instance :after ((arena arena*) &key)
  (assert (slot-boundp arena 'uuid))
  (setf *arena* arena)
  (resize arena *width* *height*)
  (bind-event arena '(:space) 'spacebar)
  (bind-event arena '(:return) 'spacebar)
  (bind-event arena '(:pageup) 'select-variation)
  (bind-event arena '(:escape) 'setup)
  (bind-event arena '(:m :control) 'toggle-music)
  (bind-event arena '(:q :control) 'quit-game)
  (setf *inhibit-splash-screen* t))

;;; Sound effects and music resources

(defresource "serve.wav" :volume 23)
(defresource "grab.wav" :volume 23)

(defresource "beatdown.ogg" :volume 63)
(defresource "rhythm.ogg" :volume 28)
(defresource "fanfare-1.ogg" :volume 50)
(defresource "fanfare-2.ogg" :volume 50)
(defresource "vixon.ogg" :volume 50)
(defresource "end.ogg" :volume 50)

(defresource "bounce.wav" :volume 10)
(defresource "newball.wav" :volume 20)
(defresource "return.wav" :volume 20)
(defresource "error.wav" :volume 40)

(defresource 
      (:name "boop1.wav" :type :sample :file "boop1.wav" :properties (:volume 20))
      (:name "boop2.wav" :type :sample :file "boop2.wav" :properties (:volume 20))
    (:name "boop3.wav" :type :sample :file "boop3.wav" :properties (:volume 20)))

(defresource 
    (:name "doorbell1.wav" :type :sample :file "doorbell1.wav" :properties (:volume 23))
    (:name "doorbell2.wav" :type :sample :file "doorbell2.wav" :properties (:volume 23))
  (:name "doorbell3.wav" :type :sample :file "doorbell3.wav" :properties (:volume 23)))

(defresource 
    (:name "slam1.wav" :type :sample :file "slam1.wav" :properties (:volume 52))
    (:name "slam2.wav" :type :sample :file "slam2.wav" :properties (:volume 52))
  (:name "slam3.wav" :type :sample :file "slam3.wav" :properties (:volume 52)))

(defresource 
    (:name "whack1.wav" :type :sample :file "whack1.wav" :properties (:volume 42))
    (:name "whack2.wav" :type :sample :file "whack2.wav" :properties (:volume 42))
  (:name "whack3.wav" :type :sample :file "whack3.wav" :properties (:volume 42)))

(defresource 
    (:name "color1.wav" :type :sample :file "color1.wav" :properties (:volume 32))
    (:name "color2.wav" :type :sample :file "color2.wav" :properties (:volume 32))
  (:name "color3.wav" :type :sample :file "color3.wav" :properties (:volume 32)))

(defresource "analog-death.wav" :volume 70)

;;; Apply shading/detail to the robot

;; (defmethod %draw :after ((self robot))
;;   (with-slots (vertex-color heading kick-clock drawn-image) self
;;     (multiple-value-bind (top left right bottom)
;; 	(bounding-box self)
;;       (draw-textured-rectangle-* left top 0
;; 				 (- right left) (- bottom top)
;; 				 "robot-detail.png"
;; 				 :vertex-color vertex-color
;; 				 :angle (+ 90 (heading-degrees heading)))
;;       (when (plusp kick-clock)
;; 	(draw-textured-rectangle-* (- left (units 1))
;; 				   (- top (units 1))
;; 				   0
;; 				   60 60
;; 				   (random-choose '("field-1.png" "field-2.png"))
;; 				   :vertex-color (random-choose '("yellow" "magenta" "cyan"))
;; 				   :blend :additive :opacity 0.8)))))

(defmethod handle-point-motion ((self arena*) x y))
(defmethod press ((self arena*) x y &optional button))
(defmethod release ((self arena*) x y &optional button))
(defmethod tap ((self arena*) x y))
(defmethod alternate-tap ((self arena*) x y))

(defmethod spacebar ((arena arena*)) 
  (hide-terminal))

(defun keyboard-heading () 
  (let ((dir (arrow-keys-direction)))
    (when dir (direction-heading dir))))

;; (defmethod stick-heading ((self player-1))
;;   (or
;;    (keyboard-heading)
;;    (if (and *player-1-joystick*
;; 	    (left-analog-stick-pressed-p *player-1-joystick*))
;;        (left-analog-stick-heading *player-1-joystick*)
;;        (call-next-method))))
  
;; (defmethod stick-heading ((self thief))
;;   (when *player-2-joystick*
;;     (when (left-analog-stick-pressed-p *player-2-joystick*)
;;       (left-analog-stick-heading *player-2-joystick*))))

;; (defmethod movement-heading ((self player-1))
;;   (stick-heading self))

;; (defmethod kicking-p ((robot player-1))
;;   (or (holding-shift-p)
;;       (holding-button-p *player-1-joystick*)))

(defun play-3x0ng ()
  (setf *inhibit-splash-screen* nil)
  (setf *debug* nil)
  (setf *use-music* nil) ;; (if *netplay* nil t))
  ;; (dolist (line (split-string-on-lines *3x0ng-copyright-notice*))
  ;;   (logging line))
  ;;(show-terminal)
  ;; (unless (or *netplay* *inhibit-splash-screen*)
  ;;   (logging "PRESS [ESCAPE] KEY TO CONFIGURE PLAYERS, GAMEPADS, AND NETWORKING."))
  ;; (setf *player-1-joystick* 0)
  ;; (sdl-cffi::sdl-joystick-open 0)
  (disable-key-repeat) 
  (reset-score)
  (reset-game-clock)
  ;; (dotimes (n 5)
  ;;   (time (holding-shift-p)))
  (let ((x (make-game *variation*)))
    ;;(time (dotimes (n 300) (update x)))
    (at-next-update (switch-to-buffer x))))

(defun 3x0ng (&rest args)
  ;; (let ((f (mapcar #'find-package '(:quadrille :ffi :serve-event :ext :si :swank/rpc :swank :swank/backend :swank/ecl :mp :gray :clos))))
  ;;   (do-all-symbols (s :3x0ng)
  ;;     (let ((p (symbol-package s)))
  ;; 	(unless (find p f)
  ;; 	  (handler-case (eval `(trace ,s)) (condition (c) nil))))))
  (setf *walls* nil)
  (setf *goals* nil)
  (setf *robots* nil)
  (setf *barriers* nil)
  (setf *use-fortresses* nil)
  (setf *use-bumpers* nil)
  (setf *use-antialiased-text* nil)
  (setf *frame-rate* 60)
  (setf *variation* (if *bricks-p* 2 1))
  (setf *font-texture-scale* 1)
  (setf *font-texture-filter* :linear)
  (setf *window-title* *title-string*)
  (setf *screen-width* *width*)
  (setf *screen-height* *height*)
  (setf *nominal-screen-width* *width*)
  (setf *nominal-screen-height* *height*)
  (setf *scale-output-to-window* t) 
  (with-session
      (setf xelf::*project-directories*
	    '(#P"/data/data/org.lisp.ecl/app_resources/home/systems/"))
    (open-project "3x0ng")
    (index-all-images)
    ;;(index-all-samples)
    (index-pending-resources)
    (preload-resources)
    ;; preload music 
    (setf *default-texture-filter* :nearest)
    ;;(mapc #'find-resource '("rhythm.ogg" "fanfare-1.ogg" "fanfare-2.ogg" "vixon.ogg" "end.ogg" "beatdown.ogg"))
    ;; (initialize-sounds)
    (play-3x0ng)))

;; (proclaim '(optimize (speed 3) (safety 1)))
;; (defclass ip-prompt (prompt)
;;   ((prompt-string :initform "Type the IP server address and then press ENTER.")))

;; (defmethod read-expression ((prompt ip-prompt) input-string)
;;   input-string)

;; (defmethod enter :before ((prompt ip-prompt) &optional no-clear)
;;   (handler-case 
;;       (let ((*read-eval* nil))
;; 	(let ((result (parse-ip (slot-value prompt 'line))))
;; 	  (if (null result)
;; 	      (logging "Error: not a valid IP address.")
;; 	      (progn 
;; 		(setf *server-host* (reformat-ip result))
;; 		(start-client (current-buffer))))))
;;     (condition (c)
;;       (logging "~S" c))))

;; (defun show-prompt ()
;;   (show-terminal)
;;   (setf *prompt* (make-instance 'ip-prompt))
;;   (move-to *prompt* *terminal-left* *terminal-bottom*))

;; (defun hide-prompt ()
;;   (setf *prompt* nil))

;;; 3x0ng.lisp ends here
